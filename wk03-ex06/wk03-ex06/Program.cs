﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk03_ex06
{
    class Program
    {
        static void Main(string[] args)
        {
            var colors = new List<string> { "red", "blue", "orange", "white", "black" };
            colors.Add("green");
            colors.Add("purple");
            colors.Add("gray");
            colors.Add("yellow");
            colors.Add("violet");

            Console.WriteLine(string.Join(",", colors));
        }
    }
}
